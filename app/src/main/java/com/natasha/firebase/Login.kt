package com.natasha.firebase

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.signup.*


class Login:AppCompatActivity() {
    var firebaseAuth: FirebaseAuth? = null
    var AuthListener: FirebaseAuth.AuthStateListener?=null

    override fun onStart() {
        super.onStart()

        firebaseAuth?.addAuthStateListener(AuthListener!!)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login)
        firebaseAuth = FirebaseAuth.getInstance()


        AuthListener=  object:FirebaseAuth.AuthStateListener {
            override fun onAuthStateChanged(p0: FirebaseAuth) {
                if(firebaseAuth?.currentUser!=null)
                {

                    startActivity(Intent(this@Login, ReadWrite::class.java))
                }
            }



        }
        var btn=findViewById<Button>(R.id.login).setOnClickListener {

            EmailPassSignIn()
        }
        var signup=findViewById<Button>(R.id.loginsignup).setOnClickListener{

            startActivity(Intent(this@Login, SignUp::class.java))
        }
    }

    private fun EmailPassSignIn() {
        val  inputEmail: EditText? = findViewById<EditText>(R.id.Email)
        val inputPassword =  findViewById<EditText>(R.id.passwordlogin)
        val email_text = inputEmail?.editableText.toString().trim()
        val password_text = inputPassword?.text.toString().trim()
        if (TextUtils.isEmpty(email_text)) {
            inputEmail?.error = "Please enter email"
            inputEmail?.requestFocus()
            return
        } else {
            if (!Patterns.EMAIL_ADDRESS.matcher(email_text).matches()) {

                inputEmail?.error = "Wrong Email format"
                inputEmail?.requestFocus()
                return
            }
        }
        if (TextUtils.isEmpty(password_text)) {
            inputPassword.error = "Please enter password"
            inputPassword.requestFocus()
            return
        }

        //progressBar.setVisibility(View.VISIBLE);
        //authenticate user
        firebaseAuth!!.signInWithEmailAndPassword(email_text, password_text)
            .addOnCompleteListener(this@Login, OnCompleteListener<AuthResult>( {

                //progressBar.setVisibility(View.GONE);
                if (it.isSuccessful()) {
                    // there was an error
                    Log.d("", "signInWithEmail:success")
                    val curruser = FirebaseAuth.getInstance().currentUser
                    if(curruser!=null) {
                        startActivity(Intent(this@Login, ReadWrite::class.java).putExtra("id", curruser?.uid))
                        startActivity(intent)
                        finish()
                    }
                } else {
                    Log.e("", "onComplete: Failed=" + it.getException()?.message)
                    Log.d("", "singInWithEmail:Fail")
                    Toast.makeText(this@Login, Library.Email_SignIn_Failed, Toast.LENGTH_LONG).show()
                }

            }))

    }
}
