package com.natasha.firebase.Models

import com.google.firebase.database.IgnoreExtraProperties
import java.lang.reflect.Constructor

@IgnoreExtraProperties
data class User(
    var name: String?="" ,
    var email: String="",
    var password:String="") {

}
@IgnoreExtraProperties
data class Users(var users:ArrayList<Map<String, User>>? = null)