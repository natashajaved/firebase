package com.natasha.firebase

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.EditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

import com.natasha.firebase.Models.Post


class ReadWrite:AppCompatActivity() {

    var firebaseAuth: FirebaseAuth? = FirebaseAuth.getInstance()

    var AuthListener: FirebaseAuth.AuthStateListener?= FirebaseAuth.AuthStateListener {  }
    override fun onStart() {
        super.onStart()

        firebaseAuth?.addAuthStateListener(AuthListener!!)

    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.postwrite)
        var i = intent
        var extras:Bundle?=null
      //  if(intent!=null) {
             extras  = getIntent().getExtras();
        //}

        AuthListener=  object:FirebaseAuth.AuthStateListener {
            override fun onAuthStateChanged(p0: FirebaseAuth) {
                if(firebaseAuth?.currentUser==null)
                {
                    startActivity(Intent(this@ReadWrite, Login::class.java))
                }
            }
        }

        var id:String=""
        if (extras != null) {
             id = extras.getString("id");

        }
        var myRef = FirebaseDatabase.getInstance().reference
        var write = findViewById<EditText>(R.id.writep)
        var read = findViewById<EditText>(R.id.retrievedData)
        var btn = findViewById<Button>(R.id.postbtn)
        var signoutbtn=findViewById<Button>(R.id.signout).setOnClickListener{
            firebaseAuth?.signOut()
        }
        btn.setOnClickListener {
            Log.d("", id)
            var key = myRef.child("Users").child(id).push().key
            myRef.child("Users").child(id).child(key!!).setValue(write.text.toString().trim()).addOnSuccessListener {


            }
                .addOnFailureListener {



            }



            myRef.child("Users").child(id).child(key!!).addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    // This method is called once with the initial value and again
                    // whenever data at this location is updated.
                    if (dataSnapshot.exists()) {

                        for (childSnapshot in dataSnapshot.getChildren()) {

                            var post = childSnapshot.getValue(Post::class.java)
                            read.setText(post!!.message)


                        }
                    }
                    else
                    {

                    }
                }


                override fun onCancelled(p0: DatabaseError) {

                }
            })
        }
    }
}