package com.natasha.firebase

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import android.support.v7.app.AppCompatActivity
import com.google.firebase.database.FirebaseDatabase
import com.natasha.firebase.Models.Post
import com.natasha.firebase.Models.User




class SignUp:AppCompatActivity() {
    var mAuth = FirebaseAuth.getInstance()
    // var listener: FragmentActivity? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.signup)

        var name = findViewById<EditText>(R.id.name)
        var email = findViewById<EditText>(R.id.emailSignup)
        var password = findViewById<EditText>(R.id.password)
        var btn = findViewById<Button>(R.id.signup)

        btn.setOnClickListener {


            if (TextUtils.isEmpty(name.getText().toString())) {
                name.error = "Please enter name"
                name.requestFocus()
                return@setOnClickListener
            }
            if (TextUtils.isEmpty(email.getText().toString())) {
                email.error = "Please enter email"
                email.requestFocus()
                return@setOnClickListener
            } else {
                //   if (!Patterns.EMAIL_ADDRESS.matcher(email.text.toString()).matches()) {

                //     email.error = "Wrong Email format"
                //   email.requestFocus()
                //  return@setOnClickListener
                //}
            }
            if (TextUtils.isEmpty(password.getText().toString())) {
                password.error = "Please enter password"
                password.requestFocus()
                return@setOnClickListener
            }

            mAuth.createUserWithEmailAndPassword(email.text.toString().trim(), password.text.toString().trim()).addOnCompleteListener(this@SignUp, object : OnCompleteListener<AuthResult> {
                    override fun onComplete(task: Task<AuthResult>) {
                        //progressBar.setVisibility(View.GONE)
                        if (task.isSuccessful()) { // Sign in success, update UI with the signed-in user's information
                            Log.d("", "createUserWithEmail:success")
                            val user = mAuth.getCurrentUser()

                            val curruser = FirebaseAuth.getInstance().currentUser
                            val ref = FirebaseDatabase.getInstance().getReference()
                            //database.child("users").child(userId).setValue(user)
                            if (curruser != null) {
                                var id = curruser?.uid
                                val user = User(name.text.toString(), email.text.toString(), password.text.toString())
                                val users = ref.child("Users").child(id).setValue(user)
                                    .addOnSuccessListener {
                                     val key=   ref.child("Posts").child(curruser.uid).push().key
                                        ref.child("Posts").child(id).child(key!!).setValue(Post())
                                        Toast.makeText(
                                            this@SignUp, "Succesfully Registered",
                                            Toast.LENGTH_SHORT
                                        ).show()
                                        startActivity(
                                            Intent(this@SignUp, ReadWrite::class.java).putExtra("id", curruser?.uid))


                                    }
                                    .addOnFailureListener {
                                        Toast.makeText(
                                            this@SignUp, "Sorry, some error occured while registration",
                                            Toast.LENGTH_SHORT
                                        ).show()

                                    }
                            }

                        }
                        else
                        {
                            Toast.makeText(
                                this@SignUp, "No Such Authentication found",
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                    }
                })
        }

        //}
    }
}











