package com.natasha.firebase

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.natasha.firebase.Models.postrecycler

class RecyclerAdapter(private val postsList:ArrayList<postrecycler>):RecyclerView.Adapter<RecyclerAdapter.PostHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerAdapter.PostHolder {
        val inflatedView = LayoutInflater.from(parent.context).inflate(R.layout.recycler_row,parent , false)
        return PostHolder(inflatedView)
    }

    override fun getItemCount(): Int = postsList.size

    override fun onBindViewHolder(holder: RecyclerAdapter.PostHolder, position: Int) {
       holder.username.text = postsList[position].username
        holder.post.text = postsList[position].post

    }

    class PostHolder(itemView: View): RecyclerView.ViewHolder(itemView), View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        private var view: View = itemView //

        var username: TextView = view.findViewById(R.id.Username)
        var post: TextView = view.findViewById(R.id.post)
        override fun onClick(p0: View?) {
//            Log.d("RECYCLERVIEW","CLICKED")
            val context = itemView.context
         //   val detailsIntent = Intent(context, DetailsActivity::class.java)
           // detailsIntent.putExtra(PHOTO_KEY, photo)
            //context.startActivity(detailsIntent)
        }

     /*   fun bindPhoto(photo: Photo) {
            this.photo = photo
            Picasso.with(view.context).load(photo.url).into(view.itemImage)
            view.itemDate.text = photo.humanDate
            view.itemDescription.text = photo.explanation
        }
*/
    }
}